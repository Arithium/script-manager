package script.listener;

import event.NpcClickEvent;
import event.ObjectClickEvent;

public abstract class NpcClickScriptListener implements ScriptListener<NpcClickEvent> {
	
	private final int id;
	
	public NpcClickScriptListener(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public boolean shouldRun(NpcClickEvent e) {
		return e.getId() == id;
	}
	
}
