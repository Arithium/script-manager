package script.listener;

import event.ObjectClickEvent;

public abstract class ObjectClickScriptListener implements ScriptListener<ObjectClickEvent> {

	private final int id;

	public ObjectClickScriptListener(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public boolean shouldRun(ObjectClickEvent e) {
		return e.getId() == id;
	}

}
