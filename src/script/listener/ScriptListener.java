package script.listener;

import event.Event;

/**
 * Represents a script
 * 
 * @author Arithium
 *
 */
public interface ScriptListener <T extends Event> {
	
	public void execute(T event);
	
	public boolean shouldRun(T event);

}
