package script;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import event.Event;
import event.NpcClickEvent;
import script.listener.NpcClickScriptListener;
import script.listener.ScriptListener;

/**
 * Loads scripts from a directory
 * 
 * @author Arithium
 *
 */
public class ScriptManager {

	/**
	 * A list of events for the scripts
	 */
	private static List<ScriptListener> scripts = new ArrayList<>();

	/**
	 * Loads all of the scripts from the directory
	 */
	public static void load() {

		System.out.println("Loading scripts.");
		String shorten = "script.";

		final Path sourceDir = Paths.get("./bin/script/scripts/");
		try {
			Files.walkFileTree(sourceDir, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					try {

						Class<?>[] classes = getClasses(shorten + "" + dir.getFileName());

						for (Class<?> clazz : classes) {
							if (ScriptListener.class.isAssignableFrom(clazz)) {
								ScriptListener e = (ScriptListener) clazz.newInstance();
								scripts.add(e);
								System.out.println("Adding new script: " + e);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 *
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private static Class<?>[] getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 *
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}
	
	public static <T extends Event> void trigger(T event, Class<?> clazz) {
		List<ScriptListener> events = ScriptManager.getSubsequentScripts(clazz);
		events.stream().forEach(i -> {
			if (i.shouldRun(event)) {
				i.execute(event);
			}
		});
	}

	public static List<ScriptListener> getSubsequentScripts(Class<?> class1) {
		return scripts.stream().filter(i -> i.getClass().getSuperclass().equals(class1)).collect(Collectors.toList());
	}

	public static List<ScriptListener> getScripts() {
		return scripts;
	}

}
