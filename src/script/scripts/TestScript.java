package script.scripts;
import event.ObjectClickEvent;
import script.listener.ObjectClickScriptListener;

public class TestScript extends ObjectClickScriptListener {
	
	public TestScript() {
		super(1);
	}

	@Override
	public void execute(ObjectClickEvent event) {
		System.out.println("Object click: " + event.getId());
	}

}
