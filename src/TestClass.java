import event.NpcClickEvent;
import event.ObjectClickEvent;
import script.ScriptManager;
import script.listener.NpcClickScriptListener;
import script.listener.ObjectClickScriptListener;

public class TestClass {
	
	public static void main(String[] args) throws Exception {
		
		ScriptManager.load();
		
		
		doObjectEvents();
		
		doNpcEvents();
	}

	private static void doNpcEvents() {
		ScriptManager.trigger(new NpcClickEvent(2), NpcClickScriptListener.class);
	}

	private static void doObjectEvents() {
		ScriptManager.trigger(new ObjectClickEvent(1, 1), ObjectClickScriptListener.class);
	}

}
