package event;

public class ObjectClickEvent implements Event {
	
	private final int id;
	private final int type;
	
	public ObjectClickEvent(int id, int type) {
		this.id = id;
		this.type = type;
	}
	
	public int getId() {
		return id;
	}
	
	public int getType() {
		return type;
	}

}
