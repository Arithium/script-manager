package event;

public class NpcClickEvent implements Event {
	
	private final int id;
	
	public NpcClickEvent(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}
